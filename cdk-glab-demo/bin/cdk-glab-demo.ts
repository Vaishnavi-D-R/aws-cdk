import { App } from '@aws-cdk/core';
import { CdkGlabDemoPipelineStack } from '../lib/cdk-glab-demo-pipeline-stack';

const app = new App();

new CdkGlabDemoPipelineStack(app, 'CdkGlabDemoPipelineStack', {
	  env: { account: '294649033237', region: 'us-east-2' },
});

app.synth();
