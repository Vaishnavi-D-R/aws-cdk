import { CfnOutput, Construct, Stage, StageProps } from '@aws-cdk/core';
import { CdkpipelinesDemoStack } from './cdk-glab-demo-stack;

/**
 * Deployable unit of web service app
 */
export class CdkGlabpipelinesDemoStage extends Stage {
	  public readonly urlOutput: CfnOutput;
	    
	    constructor(scope: Construct, id: string, props?: StageProps) {
		        super(scope, id, props);

			    const service = new CdkGlabDemoStack(this, 'WebService');
			        
			        // Expose CdkpipelinesDemoStack's output one level higher
				    this.urlOutput = service.urlOutput;
				      }
}
