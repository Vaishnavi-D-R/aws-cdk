import { CfnOutput, Construct, Stage, StageProps } from '@aws-cdk/core';
import { CdkpipelinesDemogitlabStack } from './cdkpipeliness-demogitlab-stack';

/**
 * Deployable unit of web service app
 */
export class CdkpipelinesDemogitlabStage extends Stage {
	  public readonly urlOutput: CfnOutput;
	    
	    constructor(scope: Construct, id: string, props?: StageProps) {
		        super(scope, id, props);

			    const service = new CdkpipelinesDemogitlabStack(this, 'WebService');
			        
			        // Expose CdkpipelinesDemoStack's output one level higher
				    this.urlOutput = service.urlOutput;
				      }
}
